var G = require('./Global')();
// function Critter() {
// this.direction = G.randomElement(G.directionNames);
// };
// Critter.prototype.act = function(view) {
// if (view.look(this.direction) != " ")
// this.direction = view.find(" ") || "s";
// return {type: "move", direction: this.direction};
// };
// module.exports = Critter;

function PlantEater() {
this.energy = 20;
}
PlantEater.prototype.act = function(context) {
var space = context.find(" ");
if (this.energy > 60 && space)
return {type: "reproduce", direction: space};
var plant = context.find("*");
if (plant )
return {type: "eat", direction: plant};
if (space)
return {type: "move", direction: space};
};
PlantEater.prototype.toString = function(){
	return "PlantEater";
}
module.exports = PlantEater;