function prepend(ele,list) {
  var obj ={};
  obj.value = ele;
  obj.rest = list;
  return obj;
}
function nth(list,n) {
  var count = 0;
  for (var i = list; i; i = i.rest ){
    if(n == count) return i;
    count++;
  }
  return undefined
}
function nthr(list,n) {
  if(!list) return undefined;
  if(n == 0) return list;
  return nthr(list.rest,n-1);
}
function arrayToList(array) {
  var list = null;
  for (var i = array.length - 1; i >= 0; i--)
    list = {value: array[i], rest: list};
  return list;
}
function listToArray(list) {
  var a = [];
  for (var i = list; i; i = i.rest ) {
    a.push(i.value)
  }
  return a;

}
var atl = arrayToList([1,"egg","me"])
// console.log(listToArray(atl))
// console.log(prepend("ester",atl))
console.log(nthr(atl,2))
