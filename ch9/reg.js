
console.log(/\d+/.exec("'123'"));
console.log(/\bcat\b/.test("con cat enate"));
var animalCount = /(\d+)--(pig|cow|chicken)s?\b/;
console.log(animalCount.test("the 15-pigs 17--c"));
var a = /\bfbi\b/g
console.log(a.test("the cia and fbi"));
var s = "the cia and fbi";
console.log(
"Hopper, Grace\nMcCarthy, John\nRitchie, Dennis"
.replace(/([\w ]+), ([\w ]+)/g, "$2, $1"));
var quotedText = /'([^']*)'/;
console.log(quotedText.exec("she said 'hello'"));
