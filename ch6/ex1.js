function Vector(x,y) {
  if(typeof x =="number" && typeof y == "number"){
    this.x = x;
    this.y = y;
  }else{
    console.log("invalid type for this")
  }

}
Vector.prototype.plus = function (vector) {
  return new Vector(this.x+vector.x,this.y+vector.y)
}
Vector.prototype.minus = function (vector) {
  return new Vector(this.x-vector.x,this.y-vector.y)
}
Vector.prototype.display = function() {
  console.log(`${this.x} --- ${this.y}`)
}
Object.defineProperty(Vector.prototype,"length",{
  get: function(){
    return Pythagoras(this.x,this.y);
  }
})
function Pythagoras(x,y) {
  return Math.sqrt(x*x+y*y);

}
var v1 = new Vector(2,3);
var v2 = new Vector(3,4)
var v3 = v1.plus(v2);
v2.display();
v3.length =9;
console.log(v3["length"])
