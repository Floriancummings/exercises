function parseExpression(program) {
  program = skipSpace(program);
  var match, expr;
  if (match = /^"([^"]*)"/.exec(program)){
    console.log(`in fpe ${match}`)
    expr = {type: "value", value: match[1]};
  }
  else if (match = /^\d+\b/.exec(program)){
    console.log(`in spe ${match}`)
    expr = {type: "value", value: Number(match[0])};
  }
  else if (match = /^[^\s(),"]+/.exec(program)){
    console.log(`in lpe ${match}`)
    expr = {type: "word", name: match[0]};
  }
  else
    throw new SyntaxError("Unexpected syntax: " + program);

  return parseApply(expr, program.slice(match[0].length));
}

function skipSpace(string) {
  var first = string.search(/\S/);
  console.log(`in skipSpace ${string} ${first}`)
  if (first == -1) return "";
  return string.slice(first);
}

function parseApply(expr, program) {
  program = skipSpace(program);
  if (program[0] != "(")
    return {expr: expr, rest: program};

  program = skipSpace(program.slice(1));
  expr = {type: "apply", operator: expr, args: []};
  while (program[0] != ")") {
  /* here*/  var arg = parseExpression(program);
    expr.args.push(arg.expr);
    program = skipSpace(arg.rest);
    if (program[0] == ",")
      program = skipSpace(program.slice(1));
    else if (program[0] != ")")
      throw new SyntaxError("Expected ',' or ')'");
  }
  return parseApply(expr, program.slice(1));
}

function parse(program) {
  var result = parseExpression(program);
  console.log(`in parse ${result.rest}`);
  if (skipSpace(result.rest).length > 0)
    throw new SyntaxError("Unexpected text after program");
  return result.expr;
}

console.log(parse("if(true,false,true)"));
let a ='agasadf';
console.log(a.slice(0,a.length-1))
