var Vector = require('./Vector');
function Vars() {
	// body...
	var v = Object.create(null);
	v.randomElement = function(array) {
		return array[Math.floor(Math.random() * array.length)];
	}
	v.directions = {
		"n": new Vector( 0, -1),
		"ne": new Vector( 1, -1),
		"e": new Vector( 1, 0),
		"se": new Vector( 1, 1),
		"s": new Vector( 0, 1),
		"sw": new Vector(-1, 1),
		"w": new Vector(-1, 0),
		"nw": new Vector(-1, -1)
	};
	v.directionNames = "n ne e se s sw w nw".split(" ");
	v.dirPlus = function(dir, n) {
		var index = this.directionNames.indexOf(dir);
		return this.directionNames[(index + n + 8) % 8];
	}

	v.plan=["############################",
			"#####        @        ######",
			"##   ***                **##",
			"#   *##**         **  O  *##",
			"#    ***      O   ##**    *#",
			"#       O         ##***    #",
			"#                 ##**     #",
			"#   O        #*            #",
			"#*           #**      O    #",
			"#***         ##**    O   **#",
			"##****      ###***      *###",
			"############################"],
	v.elementFromChar =function(legend, ch) {
		if (ch == " ")
		return null;
		var element = new legend[ch]();
		element.originChar = ch;
		return element;
	}
	v.charFromElement = function(element) {
		if (element == null)
		return " ";
		else
		return element.originChar;
	}
	return v;}
module.exports = Vars;
