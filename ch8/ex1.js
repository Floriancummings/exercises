function MultiplicatorUnitFailure(a) { this.message  = a}

function primitiveMultiply(a, b) {
  if(isNaN(a) || isNaN(b) )
    throw new Error("number is nan")
  if (Math.random() < 0.5)
    return a * b;
  else
    throw new MultiplicatorUnitFailure("error occurs 50% of the time");
}

function Multiply(a, b) {
  for (;;) {
    try {
      return primitiveMultiply(a, b);
    } catch (e) {
      console.log(e.message)
      if (!(e instanceof MultiplicatorUnitFailure))
        throw e;
    }
  }
}
console.log(Multiply(2,3))
