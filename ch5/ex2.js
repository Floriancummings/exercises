const ancestry = JSON.parse(require('./ancestry'));

let byName = {};

ancestry.forEach(person=>{
  byName[person.name] = person;
});
let withMotherInInventory = ancestry.filter((person) => {
  return person.mother in byName;
});
let length = withMotherInInventory.length;

console.log(withMotherInInventory.map((person) => {
  return person.born - byName[person.mother].born
}).reduce((a,b) => {
  return a+b

})/length)
