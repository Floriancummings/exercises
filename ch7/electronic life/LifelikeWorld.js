var World = require('./World'),
	actionTypes = require('./actionTypes'),
	View = require('./View');
function LifelikeWorld(map,legend) {
	// body...
	World.call(this,map,legend);
}

LifelikeWorld.prototype = Object.create(World.prototype);
LifelikeWorld.prototype.letAct = function(critter, vector) {
var action = critter.act(new View(this, vector));
var handled = action &&
action.type in actionTypes &&
actionTypes[action.type].call(this, critter,
vector, action);
if (!handled) {
critter.energy -= 0.2;
if (critter.energy <= 0)
this.grid.set(vector, null);
}
};
module.exports = LifelikeWorld;