function urlToPath(url) {
  var path = require("url").parse(url).pathname;
  var decoded = decodeURIComponent(path);
  return "." + decoded.replace(/(\/|\\)\.\.(\/|\\|$)/g, "\\");
}
console.log(urlToPath('http://myhostname:8000/../../../etc/passwd'));
