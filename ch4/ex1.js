function range(a,b,step) {
  if(step == undefined){
    if(a<b) step=1;
    else step = -1;
  }
  var arr =[];
  if(step>0 && a<b)
    for (var i = a; i <= b; i+= step) arr.push(i);
  else
    for (var i = a; i >= b; i+= step) arr.push(i);
  return arr;
}
function sum(arr) {
  var a= 0;
  for (var i = 0; i < arr.length; i++) a+=arr[i];
  return a;
}
console.log(range(2,10,-5));
