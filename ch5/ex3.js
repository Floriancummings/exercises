const ancestry = JSON.parse(require('./ancestry'));

function groupBy(array,group) {
  let obj = {};
  for (var i = 0; i < array.length; i++) {
    var g = group(array[i])
    if(!(g in obj))
      obj[g] = []
    obj[g].push(array[i]);

  }
  return obj;
}
var groupedGeneration = groupBy(ancestry,function (person) {
    return Math.ceil(person.died /100);
});

for(var i in groupedGeneration){
  var length = groupedGeneration[i].length;
  console.log(`for ${i} generation`)
  console.log(groupedGeneration[i].map((person) => {
    return person.died - person.born
  }).reduce((a,b) => {
    return a+b
  })/length);

}
