var http = require("http");
var Router = require("./router");
var ecstatic = require("ecstatic");
var fileServer = ecstatic({root: "./public"});
var router = new Router();
http.createServer(function(request, response) {
  if (!router.resolve(request, response))
  fileServer(request, response);
}).listen(8000);
