var one = new RegExp("ca(r|t)");
console.log(one.test("concatenare"))

var two = /p(r)?op/
console.log(two.test("popcorns"))

var three = /ferr(et|y|ari)/
console.log(three.test("ferrari"))

var four = /.*?ious/
console.log(four.test("delicious"))

var five = /\s(\.|\,|\;)/
console.log(five.test(" ;"))

var six = /\w{6,}/
console.log(six.test("eafsasfa"))

var seven = /([^']*)/
console.log(seven.test(""))

var quotedText = /\b[a-df-z]+\b/i;
console.log(quotedText.test("fes"));
