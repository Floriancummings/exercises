function reverseArray(arr) {
  var a = [];
  for (var i = arr.length-1; i >=0; i--) {
    a.push(arr[i]);

  }
  return a;
}

function reverseArrayInPlace(arr) {
  for (var i = 0; i < arr.length/2; i++) {
    var temp = arr[i];
    arr[i] = arr[arr.length-1-i];
    arr[arr.length-1-i] = temp;
  }

}
var a = [1,2,3,4,5]

reverseArrayInPlace(a)
console.log(a)
